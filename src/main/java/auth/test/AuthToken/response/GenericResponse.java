package auth.test.AuthToken.response;

public class GenericResponse<T> {
    public int code;
    public String message;
    public T content;

    public GenericResponse(int code, String message, T content) {
        this.code = code;
        this.message = message;
        this.content = content;
    }

    public GenericResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
