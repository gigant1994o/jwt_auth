package auth.test.AuthToken.response.auth;

import auth.test.AuthToken.model.ApplicationUser;

public class SignUpResponse {
    private ApplicationUser user;

    public SignUpResponse(ApplicationUser user) {
        this.user = user;
    }

    public ApplicationUser getUser() {
        return user;
    }
}
