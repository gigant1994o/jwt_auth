package auth.test.AuthToken.controller;


import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/data")
public class DataController {

    @RequestMapping("/test")
    public String test(Authentication authentication){
        return authentication.getPrincipal().toString();
    }
    @RequestMapping("/test2")
    public String test2(Authentication authentication){
        return "Barev";
    }
}
