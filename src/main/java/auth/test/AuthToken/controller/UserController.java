package auth.test.AuthToken.controller;

import auth.test.AuthToken.model.ApplicationUser;
import auth.test.AuthToken.response.GenericResponse;
import auth.test.AuthToken.response.auth.SignUpResponse;
import auth.test.AuthToken.service.auth.AuthService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {
    private AuthService authService;

    public UserController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/sign-up")
    public ResponseEntity<GenericResponse<?>> signUp(@RequestBody ApplicationUser user) {
        ApplicationUser savedUser = authService.registerUser(user);
        if (savedUser != null) {
            return new ResponseEntity<>(new GenericResponse<>(
                    HttpStatus.CREATED.value(), HttpStatus.CREATED.getReasonPhrase(), new SignUpResponse(savedUser)),
                    HttpStatus.CREATED);
        }
        return new ResponseEntity<>(
                new GenericResponse<String>(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase()),
                HttpStatus.CREATED);
    }

}