package auth.test.AuthToken.service.auth;

import auth.test.AuthToken.model.ApplicationUser;
import auth.test.AuthToken.repo.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    private final ApplicationUserRepository applicationUserRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    @Autowired
    public AuthService(ApplicationUserRepository applicationUserRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public ApplicationUser registerUser(ApplicationUser user) {
        if (isUserValid(user)) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            return applicationUserRepository.save(user);
        }
        return null;
    }

    private boolean isUserValid(ApplicationUser user) {
        return isValidPassword(user.getPassword()) && isValidUsername(user.getUsername());
    }

    private boolean isValidUsername(String username) {
        return username != null &&
                applicationUserRepository.findByUsername(username) == null && username.length() > 2;
    }

    private boolean isValidPassword(String password) {
        if (password == null) {
            return false;
        }
        return password.length() > 3;
    }

}
